
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.*;

class Main {

	public static void main(String[] args) throws MalformedURLException, IOException {

		// create url
        String weather_state_name = "mumbai";
        URL url = new URL("https://www.metaweather.com/api/location/search/?query="+weather_state_name);
        
		// Send Get request and fetch data
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");

		BufferedReader br = new BufferedReader(new InputStreamReader(
            (conn.getInputStream())));
            

		// Read data line-by-line from buffer & print it out

        String output;
        String[] outputArray = new String[20];
        int i=0;
		while ((output = br.readLine()) != null) {
            System.out.println(output);
            outputArray[i++] = output;
        }

        
        //Extract woeid using Regex

        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(outputArray[0]);
        String woeid_string = m.find()? m.group() : null;
        int woeid = Integer.parseInt(woeid_string);

        //Now print the day's weather
        URL url_weather = new URL("https://www.metaweather.com/api/location/"+woeid+"/");

        String str = " ";
        StringBuilder jsonString = new StringBuilder();
        try(BufferedReader in = new BufferedReader(new InputStreamReader(url_weather.openStream()))) {
        while ((str = in.readLine()) != null) {
        System.out.println(str);
        jsonString.append(str).append("\n");
        }

}

        //Convert string to json object using GSON

		conn.disconnect();

	}

}
